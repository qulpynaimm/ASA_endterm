FROM node:18-alpine

WORKDIR /app

COPY backend/package.json .

RUN npm install

COPY . .

CMD ["node", "backend/app.js"]

EXPOSE 80
